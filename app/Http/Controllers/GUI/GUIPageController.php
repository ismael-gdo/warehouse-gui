<?php namespace App\Http\Controllers\GUI;

use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Aisle;
use App\Models\Bin;
use App\Models\BinLevel;
use App\Models\DrShipmentItemLocation;
use App\Models\Level;
use App\Models\PpTable;
use App\Models\PickAisle;
use App\Models\Section;

class GUIPageController extends Controller {

	public function __construct()
  {

  }

	/**
	 * Creates the interface main view
	 * @param  [string] $area [Warehouse area name]
	 * @return view
	 */
  public function index($area) {

		$pageTitle = 'Inventory Interface';

		// Predetermined inventory areas list taken from route, areas can be added
		// TODO Create areas document
		$existingAreas = ['replenish', 'pick'];

		// Include area name in title if it exists
		if(in_array($area, $existingAreas)) {
			$pageTitle = ucwords($area) . ' ' . $pageTitle;
		}

		// If area exists fetch requested area data
		switch($area) {
			case 'pick':
				$inventoryData = $this->_fetchPickData($area);
				break;
			case 'replenish':
				$inventoryData = $this->_fetchReplenishData($area);
				break;
			default:
				$inventoryData = json_encode(['success' => false, 'message' => 'Warehouse area "' . ucwords($area) . '" does not exist.', 'data' => NULL]);
		}

    return view('warehouse-gui.index', compact('pageTitle', 'inventoryData'));
  }

	/**
	 * Fetch inventory summary per aisle for Pick
	 * @return json
	 */
	public function _fetchPickData($area) {

		try {

			// Create return data array container
			$inventoryByAisle = [];

			// Get all available pick aisles
			$pickAisles = PickAisle::select(['id', 'name'])->get();

			// Create array container for each available aisle
			foreach($pickAisles as $pickAisle) {

				$companyNames = PpTable::selectRaw('DISTINCT (SELECT company_name FROM customers WHERE id = customer_id ) AS company_name')->
												where('pick_aisle_id', '=', $pickAisle->id)->
												get();

				$sections = Section::selectRaw('id, section_number AS section')->where('pick_aisle_id', '=', $pickAisle->id)->get();

				$pickAisleData = PpTable::selectRaw("
					SUM(item_quantity) AS total_inventory,
					(SELECT COUNT(id) FROM levels WHERE pick_aisle_id = $pickAisle->id) AS total_available_levels,
					(SELECT COUNT(id) FROM sections WHERE pick_aisle_id = $pickAisle->id) AS total_available_sections,
					(SELECT COUNT(DISTINCT item_sku_number) FROM pp_tables WHERE pick_aisle_id = $pickAisle->id) AS total_skus,
					(SELECT COUNT(DISTINCT customer_id) FROM pp_tables WHERE pick_aisle_id = $pickAisle->id) AS total_customers,
					(SELECT COUNT(DISTINCT section_id) FROM pp_tables WHERE pick_aisle_id = $pickAisle->id) AS total_occupied_sections,
					(SELECT COUNT(DISTINCT section_bin_id) FROM pp_tables WHERE pick_aisle_id = $pickAisle->id) AS total_occupied_levels,
					(SELECT GROUP_CONCAT(id) FROM sections WHERE pick_aisle_id = $pickAisle->id) AS aisle_sections
				")->
				where('pick_aisle_id', '=', $pickAisle->id)->
				get();

				// Create array container for current aisle data
				$aisleData = [];

				// Vue element visiblity flags
				$aisleData['customers_visible'] = false;
				$aisleData['inventory_visible'] = false;
				$aisleData['data_pane_visible'] = false;
				$aisleData['aisle_inventory'] = NULL;
				$aisleData['active_section'] = NULL;
				$aisleData['data_pane'] = NULL;

				// Fill aisle data array
				$aisleData['area'] = $area;
				$aisleData['sections'] = $sections;
				$aisleData['aisle_id'] = $pickAisle->id;
				$aisleData['aisle_name'] = $pickAisle->name;
				$aisleData['company_names'] = $companyNames;
				$aisleData['total_skus'] = $pickAisleData[0]->total_skus;
				$aisleData['total_customers'] = $pickAisleData[0]->total_customers;
				$aisleData['total_inventory'] = number_format($pickAisleData[0]->total_inventory);
				$aisleData['total_occupied_bins'] = $pickAisleData[0]->total_occupied_sections;
				$aisleData['total_occupied_levels'] = $pickAisleData[0]->total_occupied_levels;
				$aisleData['total_available_bins'] = $pickAisleData[0]->total_available_sections;
				$aisleData['total_available_levels'] = $pickAisleData[0]->total_available_levels;

				array_push($inventoryByAisle, $aisleData);

			}

			return json_encode([
				'success' => true,
				'message' => 'Displaying inventory information for ' . count($pickAisles) . ' ' . ucwords($area) .' aisle(s).',
				'data' => $inventoryByAisle
			]);

		} catch(QueryException $e) {

			return json_encode(['success' => false, 'message' => $e->previous->getMessage(), 'data' => NULL]);

		}

	}

	/**
	 * Fetch inventory summary per aisle for Replenish
	 * @return json
	 */
	public function _fetchReplenishData($area) {

		try {

			// Create return data array container
			$inventoryByAisle = [];

			// Get all available replenish aisles
			$repAisles = Aisle::select(['id', 'name'])->get();

			// Create array container for each available aisle
			foreach($repAisles as $repAisle) {

				$companyNames = DrShipmentItemLocation::selectRaw('DISTINCT (SELECT company_name FROM customers WHERE id = customer_id ) AS company_name')->
												where('aisle_id', '=', $repAisle->id)->
												get();

				$sections = Bin::selectRaw("id, bin_number AS section")->where('aisle_id', '=', $repAisle->id)->get();

				$itemsInRepAisle = DrShipmentItemLocation::selectRaw("
					SUM(item_count) AS total_inventory,
					(SELECT COUNT(id) FROM bins WHERE aisle_id = $repAisle->id) AS total_available_sections,
					(SELECT COUNT(id) FROM bin_levels WHERE aisle_id = $repAisle->id) AS total_available_levels,
					(SELECT COUNT(DISTINCT item_sku_number) FROM dr_shipment_item_locations WHERE aisle_id = $repAisle->id) AS total_skus,
					(SELECT COUNT(DISTINCT customer_id) FROM dr_shipment_item_locations WHERE aisle_id = $repAisle->id) AS total_customers,
					(SELECT COUNT(DISTINCT bin_id) FROM dr_shipment_item_locations WHERE aisle_id = $repAisle->id) AS total_occupied_sections,
					(SELECT COUNT(DISTINCT bin_level_id) FROM dr_shipment_item_locations WHERE aisle_id = $repAisle->id) AS total_occupied_levels,
					(SELECT GROUP_CONCAT(id) FROM bins WHERE aisle_id = $repAisle->id) AS aisle_sections
				")->
				where('aisle_id', '=', $repAisle->id)->
				get();

				// Create array container for current aisle data
				$aisleData = [];

				// Vue element visiblity flags and containers
				$aisleData['customers_visible'] = false;
				$aisleData['inventory_visible'] = false;
				$aisleData['data_pane_visible'] = false;
				$aisleData['section_inventory'] = NULL;
				$aisleData['active_section'] = NULL;
				$aisleData['data_pane'] = NULL;

				// Fill aisle data array
				$aisleData['area'] = $area;
				$aisleData['sections'] = $sections;
				$aisleData['aisle_id'] = $repAisle->id;
				$aisleData['aisle_name'] = $repAisle->name;
				$aisleData['company_names'] = $companyNames;
				$aisleData['total_skus'] = $itemsInRepAisle[0]->total_skus;
				$aisleData['total_customers'] = $itemsInRepAisle[0]->total_customers;
				$aisleData['total_inventory'] = number_format($itemsInRepAisle[0]->total_inventory);
				$aisleData['total_occupied_bins'] = $itemsInRepAisle[0]->total_occupied_sections;
				$aisleData['total_available_bins'] = $itemsInRepAisle[0]->total_available_sections;
				$aisleData['total_occupied_levels'] = $itemsInRepAisle[0]->total_occupied_levels;
				$aisleData['total_available_levels'] = $itemsInRepAisle[0]->total_available_levels;

				$inventoryByAisle[$repAisle->name] = $aisleData;

			}

			return json_encode([
				'success' => true,
				'message' => 'Displaying inventory information for ' . count($repAisles) . ' ' . ucwords($area) . ' aisle(s).',
				'data' => $inventoryByAisle
			]);

		} catch(QueryException $e) {

			return json_encode(['success' => false, 'message' => $e->previous->getMessage(), 'data' => NULL]);

		}

	}

	public function getSectionLevelsInventory($area, $sectionId) {

		$sectionData = [];

		if($area === 'replenish') {

			$levels = BinLevel::select(['id', 'level'])->where('bin_id', '=', $sectionId)->get();

			foreach($levels as $level) {

				$sectionData[$level->level]['level_id'] = $level->id;
				$sectionData[$level->level]['level_name'] = $level->level;

				$levelInventory = DrShipmentItemLocation::selectRaw('
					(SELECT company_name FROM customers WHERE id = customer_id) AS company,
					(SELECT item_name FROM customer_items WHERE item_sku_number = dr_shipment_item_locations.item_sku_number AND customer_id = dr_shipment_item_locations.customer_id) AS item_name,
					item_sku_number,
					item_count AS location_qty,
					pallet_count AS pallet,
					aisle_id AS aisle_id,
					bin_id AS section_id,
					bin_level_id AS level_id,
					(SELECT name FROM aisles WHERE id = aisle_id) AS aisle,
					(SELECT bin_number FROM bins WHERE id = bin_id) AS section,
					(SELECT level FROM levels WHERE id = bin_level_id) AS level
				')->where('bin_level_id', '=', $level->id)->get();

				$sectionData[$level->level]['level_inventory'] = $levelInventory;
			}

		} else if ($area === 'pick') {

			$levels = Level::select(['id', 'level'])->where('section_id', '=', $sectionId)->get();

			foreach($levels as $level) {

				$sectionData[$level->level]['level_id'] = $level->id;
				$sectionData[$level->level]['level_name'] = $level->level;

				$levelInventory = PpTable::selectRaw('
					(SELECT company_name FROM customers WHERE id = customer_id) AS company,
					(SELECT item_name FROM customer_items WHERE item_sku_number = pp_tables.item_sku_number AND customer_id = pp_tables.customer_id) AS item_name,
					item_sku_number,
					item_quantity AS location_qty,
					pick_aisle_id AS aisle_id,
					section_id,
					section_bin_id AS level_id,
					(SELECT name FROM aisles WHERE id = pick_aisle_id) AS aisle,
					(SELECT bin_number FROM bins WHERE id = section_id) AS section,
					(SELECT level FROM levels WHERE id = section_bin_id) AS level
				')->where('section_bin_id', '=', $level->id)->get();

				$sectionData[$level->level]['level_inventory'] = $levelInventory;
			}

		}

		return $sectionData;

	}


}
