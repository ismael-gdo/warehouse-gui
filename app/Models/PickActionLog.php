<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PickActionLog
 */
class PickActionLog extends Model
{

    protected $connection = 'mysql_2';

    protected $table = 'pick_action_log';

    public $timestamps = true;

    // Table fields 'item_sku_number',
    //              'customer_id',
    //              'action',
    //              'action_note'
    //              'created_at'
    //              'created_by'
    //              'pick_location'

    protected $fillable = [
      'action_note'
    ];

}
