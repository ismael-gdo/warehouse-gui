<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Group
 */
class Group extends Model
{
    protected $table = 'groups';

    public $timestamps = true;

    // Table fields  'name',
    //               'permissions'

    protected $guarded = [
        'name',
        'permissions'
    ];

    protected $fillable = [];


}
