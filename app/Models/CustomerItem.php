<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerItem
 */
class CustomerItem extends Model
{
    protected $table = 'customer_items';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
