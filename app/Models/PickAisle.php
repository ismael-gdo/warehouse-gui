<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PickAisle
 */
class PickAisle extends Model
{
    protected $table = 'pick_aisles';

    public $timestamps = true;

    // Table fields  'warehouse_id',
    //               'name',
    //               'created_by',
    //               'updated_by'

    protected $guarded = [
        'warehouse_id',
        'name',
        'created_by',
        'updated_by'
    ];

    protected $fillable = [];


}
