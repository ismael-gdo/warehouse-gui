<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bin
 */
class Bin extends Model
{
    protected $table = 'bins';

    public $timestamps = true;

    // Table Fields  'warehouse_id',
    //               'aisle_id',
    //               'bin_number',
    //               'created_by',
    //               'updated_by'

    protected $guarded = [
        'warehouse_id',
        'aisle_id',
        'bin_number',
        'created_by',
        'updated_by'
    ];

    protected $fillable = [];

    public function levels()
    {
      return $this->hasMany('App\Models\BinLevel');
    }

    public function aisle()
    {
      return $this->belongsTo('App\Models\Aisle');
    }


}
