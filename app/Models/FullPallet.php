<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FullPallet
 */
class FullPallet extends Model
{
    protected $table = 'full_pallets';

    public $timestamps = true;

    // Table fields  'pallet_count',
    //               'created_by'

    protected $guarded = [
        'pallet_count',
        'created_by'
    ];

    protected $fillable = [];


}
