<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DrShipmentItemLocation
 */
class DrShipmentItemLocation extends Model
{
    protected $table = 'dr_shipment_item_locations';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
