<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PpTable
 */
class PpTable extends Model
{
    protected $table = 'pp_tables';

    public $timestamps = true;

    // Table fields  'pp_request_id',
    //               'customer_id',
    //               'item_sku_number',
    //               'item_quantity',
    //               'created_by',
    //               'updated_by',
    //               'warehouse_id',
    //               'pick_aisle_id',
    //               'section_id',
    //               'section_bin_id'

    protected $guarded = [
        'pp_request_id',
        'customer_id',
        'item_sku_number',
        'item_quantity',
        'created_by',
        'updated_by',
        'warehouse_id',
        'pick_aisle_id',
        'section_id',
        'section_bin_id'
    ];

    protected $fillable = [];


}
