<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 */
class Customer extends Model
{
    protected $table = 'customers';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
