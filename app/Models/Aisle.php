<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Aisle
 */
class Aisle extends Model
{
    protected $table = 'aisles';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];

    public function bins()
    {

      return $this->hasMany('App\Models\Bin');

    }


}
