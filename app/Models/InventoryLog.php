<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InventoryLog
 */
class InventoryLog extends Model
{
    protected $table = 'inventory_logs';

    protected $guarded = [];

    protected $fillable = [];


}
