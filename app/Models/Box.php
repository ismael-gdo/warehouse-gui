<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Box
 */
class Box extends Model
{
    protected $table = 'boxes';

    public $timestamps = true;

    // Table Fields  'order_id',
    //               'product_id',
    //               'item_name',
    //               'item_sku',
    //               'ordered_quantity',
    //               'remaining_quantity',
    //               'box_quantity',
    //               'box_number',
    //               'created_by',
    //               'tracking_code',
    //               'updated_by',
    //               'city_bin_id',
    //               'master_box_code'

    protected $guarded = [
        'order_id',
        'product_id',
        'item_name',
        'item_sku',
        'ordered_quantity',
        'remaining_quantity',
        'box_quantity',
        'box_number',
        'created_by',
        'tracking_code',
        'updated_by',
        'city_bin_id',
        'master_box_code'
    ];

    protected $fillable = [];


}
