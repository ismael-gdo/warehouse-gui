<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Level
 */
class Level extends Model
{
    protected $table = 'levels';

    public $timestamps = true;

    // Table fields  'warehouse_id',
    //               'pick_aisle_id',
    //               'section_id',
    //               'level',
    //               'created_by',
    //               'updated_by'

    protected $guarded = [
        'warehouse_id',
        'pick_aisle_id',
        'section_id',
        'level',
        'created_by',
        'updated_by'
    ];

    protected $fillable = [];


}
