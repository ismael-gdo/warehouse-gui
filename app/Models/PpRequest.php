<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PpRequest
 */
class PpRequest extends Model
{
    protected $table = 'pp_requests';

    public $timestamps = true;

    // Table fields  'customer_id',
    //               'item_sku_number',
    //               'requested_quantity',
    //               'request_urgency_status',
    //               'request_status',
    //               'created_by',
    //               'updated_by'

    protected $guarded = [
        'customer_id',
        'item_sku_number',
        'requested_quantity',
        'request_urgency_status',
        'request_status',
        'created_by',
        'updated_by'
    ];

    protected $fillable = [];


}
