<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Section
 */
class Section extends Model
{
    protected $table = 'sections';

    public $timestamps = true;

    // Table fields  'warehouse_id',
    //               'pick_aisle_id',
    //               'section_number',
    //               'created_by',
    //               'updated_by'

    protected $guarded = [
        'warehouse_id',
        'pick_aisle_id',
        'section_number',
        'created_by',
        'updated_by'
    ];

    protected $fillable = [];

    public function levels()
    {
        return $this->hasMany('App\Level');
    }

}
