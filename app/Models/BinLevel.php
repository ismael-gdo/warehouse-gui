<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BinLevel
 */
class BinLevel extends Model
{
    protected $table = 'bin_levels';

    public $timestamps = true;

    // Table fields  'warehouse_id',
    //               'aisle_id',
    //               'bin_id',
    //               'level',
    //               'created_by',
    //               'updated_by'

    protected $guarded = [
        'warehouse_id',
        'aisle_id',
        'bin_id',
        'level',
        'created_by',
        'updated_by'
    ];

    protected $fillable = [];

    public function bin()
    {
      return $this->belongsTo('App\Models\Bin');
    }


}
