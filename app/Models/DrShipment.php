<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DrShipment
 */
class DrShipment extends Model
{
    protected $table = 'dr_shipments';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
