<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Warehouse
 */
class Warehouse extends Model
{
    protected $table = 'warehouses';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
