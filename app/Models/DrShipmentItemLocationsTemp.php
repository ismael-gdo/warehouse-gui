<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DrShipmentItemLocationsTemp
 */
class DrShipmentItemLocationsTemp extends Model
{
    protected $table = 'dr_shipment_item_locations_temp';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
