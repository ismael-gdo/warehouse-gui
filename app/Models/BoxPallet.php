<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BoxPallet
 */
class BoxPallet extends Model
{
    protected $table = 'box_pallets';

    public $timestamps = true;

    // Table fields  'pallet_number',
    //               'box_number',
    //               'box_type',
    //               'city_bin_id',
    //               'is_pallet_completed',
    //               'barcode_number',
    //               'barcode_file_name',
    //               'box_pallet_status',
    //               'created_by',
    //               'updated_by',
    //               'box_pallet_tracking_code'

    protected $guarded = [
        'pallet_number',
        'box_number',
        'box_type',
        'city_bin_id',
        'is_pallet_completed',
        'barcode_number',
        'barcode_file_name',
        'box_pallet_status',
        'created_by',
        'updated_by',
        'box_pallet_tracking_code'
    ];

    protected $fillable = [];


}
