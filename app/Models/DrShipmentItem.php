<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DrShipmentItem
 */
class DrShipmentItem extends Model
{
    protected $table = 'dr_shipment_items';

    public $timestamps = true;

    protected $guarded = [];

    protected $fillable = [];


}
