<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id')->unsgined();
			$table->string('item_sku_number');
			$table->string('item_name');
			$table->string('item_description');
			$table->integer('item_reorder_trigger');
			$table->enum('item_status', array('1', '0'))->default('1');
			$table->string('item_image');
			$table->timestamps();
			$table->integer('created_by')->unsgined();
			$table->integer('updated_by')->unsgined();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_items');
	}

}
