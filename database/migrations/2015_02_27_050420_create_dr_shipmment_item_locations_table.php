<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrShipmmentItemLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dr_shipment_item_locations', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('customer_id')->unsigned();
			$table->string('item_sku_number');
			$table->integer('item_count')->default(0);
			$table->integer('pallet_count')->default(0);
			$table->integer('warehouse_id')->unsigned();
			$table->integer('aisle_id')->unsigned();
			$table->integer('bin_id')->unsigned();
			$table->integer('bin_level_id')->unsigned();
			$table->timestamps();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dr_shipment_item_locations');
	}

}
