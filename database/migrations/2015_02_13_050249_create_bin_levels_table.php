<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bin_levels', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('warehouse_id')->unsigned();
			$table->integer('aisle_id')->unsigned();
			$table->integer('bin_id')->unsigned();
			$table->string('level');
			$table->timestamps();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bin_levels');
	}

}
