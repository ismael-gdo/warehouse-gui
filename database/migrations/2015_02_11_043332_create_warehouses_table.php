<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('warehouses', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->string('warehouse_name');
			$table->string('warehouse_address1');
			$table->string('warehouse_address2');
			$table->string('warehouse_city');
			$table->string('warehouse_state');
			$table->string('warehouse_zip');
			$table->string('warehouse_country');
			$table->string('warehouse_phone1');
			$table->string('warehouse_phone2');
			$table->string('manager_name');
			$table->string('manager_email');
			$table->string('manager_phone1');
			$table->string('manager_phone2');
			$table->string('manager_address1');
			$table->string('manager_address2');
			$table->string('manager_city');
			$table->string('manager_state');
			$table->string('manager_zip');
			$table->string('manager_country');
			$table->integer('default_bin');
			$table->integer('default_bin_level');
			$table->timestamps();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('warehouses');
	}

}
