<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('parent_id')->unsigned();
			$table->string('company_name');
			$table->string('company_alias')->nullable();
			$table->text('company_emails');
			$table->string('company_address1');
			$table->string('company_address2');
			$table->string('company_city');
			$table->string('company_state');
			$table->string('company_zip');
			$table->string('company_country');
			$table->string('contact_fname');
			$table->string('contact_mname')->nullable();
			$table->string('contact_lname');
			$table->string('contact_phone_primary');
			$table->string('contact_phone_secondary')->nullable();
			$table->string('contact_fax')->nullable();
			$table->string('contact_email');
			$table->timestamps();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
