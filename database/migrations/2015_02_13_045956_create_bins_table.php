<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bins', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('warehouse_id')->unsigned();
			$table->integer('aisle_id')->unsigned();
			$table->integer('bin_number');
			$table->timestamps();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bins');
	}

}
