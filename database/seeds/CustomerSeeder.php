<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerSeeder extends Seeder {

    public function run()
    {
			DB::table('customers')->truncate();

      $faker = Faker\Factory::create();

      foreach(range(1,3) as $index) {

        $company_name = $faker->company;
        $email_domain = $faker->domainName;

        $customer = new Customer;

        $customer->parent_id								  = '0';
        $customer->company_name							  = $company_name;
        $customer->company_alias						  = $company_name . ' ' . $faker->companySuffix;
        $customer->company_emails						  = $faker->userName . '@' . $email_domain;
        $customer->company_address1 					= $faker->streetAddress;
        $customer->company_address2 					= $faker->secondaryAddress;
        $customer->company_city 						  = $faker->city;
        $customer->company_state 						  = $faker->state;
        $customer->company_zip 							  = $faker->postcode;
        $customer->company_country 						= 'US';
        $customer->contact_fname 						  = $faker->firstName;
        $customer->contact_mname 						  = '';
        $customer->contact_lname 						  = $faker->lastName;
        $customer->contact_phone_primary			= $faker->phoneNumber;
        $customer->contact_phone_secondary		= $faker->phoneNumber;
        $customer->contact_fax 							  = $faker->phoneNumber;
        $customer->contact_email 						  = $faker->userName . '@' . $email_domain;
        $customer->created_by 							  = 20;
        $customer->updated_by 							  = 20;
        $customer->save();

      }

    }

}
