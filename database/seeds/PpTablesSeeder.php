<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;
use App\Models\CustomerItem;
use App\Models\PickAisle;
use App\Models\PpTable;
use App\Models\Level;

class PpTablesSeeder extends Seeder {

    public function run()
    {
        DB::table('pp_tables')->truncate();

        $faker = Faker\Factory::create();

        $customerItems = CustomerItem::get();
        $pickLevels = Level::get();

        foreach($customerItems as $item) {
            $randomLevel = $pickLevels->random();

            $pickItem = new PpTable;

            $pickItem->customer_id 		= $item->customer_id;
            $pickItem->item_sku_number = $item->item_sku_number;
            $pickItem->item_quantity = rand(1,100);
            $pickItem->warehouse_id = 1;
            $pickItem->pick_aisle_id = $randomLevel->pick_aisle_id;
            $pickItem->section_id = $randomLevel->section_id;
            $pickItem->section_bin_id = $randomLevel->id;
            $pickItem->note = $faker->sentence(10, true);
            $pickItem->created_by 			= 20;
            $pickItem->updated_by 			= 20;

            $pickItem->save();
        }
    }
}
