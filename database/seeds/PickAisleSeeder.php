<?php

use Illuminate\Database\Seeder;
use App\Models\PickAisle;

class PickAisleSeeder extends Seeder {

    public function run()
    {
        DB::table('pick_aisles')->truncate();

				$aisle = new PickAisle;

				$aisle->warehouse_id 		= 1;
				$aisle->name 				= 'Pick Aisle 1';
				$aisle->created_by 			= 20;
				$aisle->updated_by 			= 20;

				$aisle->save();

        $aisle = new PickAisle;

				$aisle->warehouse_id 		= 1;
				$aisle->name 				= 'Pick Aisle 2';
				$aisle->created_by 			= 20;
				$aisle->updated_by 			= 20;

				$aisle->save();

    }

}
