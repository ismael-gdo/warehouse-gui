<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;
use App\Models\CustomerItem;

class CustomerItemSeeder extends Seeder {

    public function run() {
  			DB::table('customer_items')->truncate();

        $faker = Faker\Factory::create();

        $customers = Customer::get();

        foreach($customers as $customer) {
            foreach(range(1,100) as $index) {
              $customerItem = new CustomerItem;

        			$customerItem->customer_id 	 						= $customer->id;
        			$customerItem->item_sku_number 	 				= $faker->unique()->ean8;
        			$customerItem->item_name	 							= $customer->company_name . ' - ' .$faker->words(3, true);
        			$customerItem->item_description  				= $faker->sentence(6, true);
        			$customerItem->item_image  							= '';
        			$customerItem->item_reorder_trigger  		= '20';
        			$customerItem->created_by  							= 20;
        			$customerItem->updated_by  							= 20;
        			$customerItem->save();

            }
        }
    }
}
