<?php

use Illuminate\Database\Seeder;
use App\Models\PickAisle;
use App\Models\Section;

class SectionSeeder extends Seeder {

    public function run() {
        DB::table('sections')->truncate();

        $pickAisles = PickAisle::get();

        foreach($pickAisles as $aisle) {

          foreach(range(1,20) as $index) {
              $section = new Section;

    					$section->warehouse_id 			= 1;
    					$section->pick_aisle_id 		= $aisle->id;
    					$section->section_number 		= $index;
    					$section->created_by 		 	= 20;
    					$section->updated_by 		 	= 20;

    					$section->save();
            }
        }
    }
}
