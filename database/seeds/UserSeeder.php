<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder {

    public function run()
    {

      DB::table('users')->truncate();

      $faker = Faker\Factory::create();

      for($i = 0; $i <= 10; $i++) {

        $section = new User;

        $section->name 			  = $faker->name;
        $section->email 		  = $faker->email;
        $section->password 	  = bcrypt('secret');
        $section->created_at  = Carbon::now();
        $section->updated_at  = Carbon::now();

        $section->save();

      }

    }

}
