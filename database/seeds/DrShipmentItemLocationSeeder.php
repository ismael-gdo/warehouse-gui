<?php

use Illuminate\Database\Seeder;
use App\Models\BinLevel;
use App\Models\CustomerItem;
use App\Models\DrShipmentItemLocation;

class DrShipmentItemLocationSeeder extends Seeder {

    public function run() {
        DB::table('dr_shipment_item_locations')->truncate();

        $faker = Faker\Factory::create();

        $customerItems = CustomerItem::get();
        $repLevels = BinLevel::get();

        foreach($customerItems as $item) {
            $randomLevel = $repLevels->random();

            $drShipmentItemLocation = new drShipmentItemLocation;

            $drShipmentItemLocation->customer_id 					= $item->customer_id;
            $drShipmentItemLocation->item_sku_number 			= $item->item_sku_number;
            $drShipmentItemLocation->item_count 					= rand(1, 1000);
            $drShipmentItemLocation->pallet_count 				= 0;
            $drShipmentItemLocation->warehouse_id 				= 1;
            $drShipmentItemLocation->aisle_id 						= $randomLevel->aisle_id;
            $drShipmentItemLocation->bin_id 							= $randomLevel->bin_id;
            $drShipmentItemLocation->bin_level_id 				= $randomLevel->id;
            $drShipmentItemLocation->created_by 					= 20;
            $drShipmentItemLocation->updated_by 					= 20;

            $drShipmentItemLocation->save();

            if(rand(1,100) < 30) {

              $randomLevel = $repLevels->random();

              $drShipmentItemLocation = new drShipmentItemLocation;

              $drShipmentItemLocation->customer_id 					= $item->customer_id;
              $drShipmentItemLocation->item_sku_number 			= $item->item_sku_number;
              $drShipmentItemLocation->item_count 					= rand(1, 1000);
              $drShipmentItemLocation->pallet_count 				= 0;
              $drShipmentItemLocation->warehouse_id 				= 1;
              $drShipmentItemLocation->aisle_id 						= $randomLevel->aisle_id;
              $drShipmentItemLocation->bin_id 							= $randomLevel->bin_id;
              $drShipmentItemLocation->bin_level_id 				= $randomLevel->id;
              $drShipmentItemLocation->created_by 					= 20;
              $drShipmentItemLocation->updated_by 					= 20;

              $drShipmentItemLocation->save();
            }
        }

    }

}
