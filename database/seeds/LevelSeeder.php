<?php

use Illuminate\Database\Seeder;
use App\Models\Level;
use App\Models\Section;

class LevelSeeder extends Seeder {

  public function run()
  {
    DB::table('levels')->truncate();

    $sections = Section::get();

    foreach($sections as $section) {
      foreach(range('a', 'c') as $index) {
				$level = new Level;

				$level->warehouse_id 		= 1;
				$level->pick_aisle_id 		= $section->pick_aisle_id;
				$level->section_id 			= $section->id;
				$level->level 				= $index;
				$level->created_by 		 	= 20;
				$level->updated_by 		 	= 20;

				$level->save();
      }
		}
  }
}
