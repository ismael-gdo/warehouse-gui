<?php

use Illuminate\Database\Seeder;
use App\Models\Aisle;


class AisleSeeder extends Seeder {

    public function run()
    {
        DB::table('aisles')->truncate();

				$aisle = new Aisle;

				$aisle->warehouse_id 		= 1;
				$aisle->name 						= 'Rep Aisle 1';
				$aisle->created_by 			= 20;
				$aisle->updated_by 			= 20;

				$aisle->save();

        $aisle = new Aisle;

				$aisle->warehouse_id 		= 1;
				$aisle->name 						= 'Rep Aisle 2';
				$aisle->created_by 			= 20;
				$aisle->updated_by 			= 20;

				$aisle->save();
    }

}
