<?php

use Illuminate\Database\Seeder;
use App\Models\Aisle;
use App\Models\Bin;

class BinSeeder extends Seeder {

    public function run() {
        DB::table('bins')->truncate();

        $aisles = Aisle::get();

        foreach($aisles as $aisle) {
          foreach(range(1,20) as $index) {
              $bin = new Bin;

              $bin->warehouse_id 		= 1;
              $bin->aisle_id 				= $aisle->id;
              $bin->bin_number 			= $index;
              $bin->created_by 		 	= 20;
              $bin->updated_by 		 	= 20;

              $bin->save();
            }
				}
    }
}
