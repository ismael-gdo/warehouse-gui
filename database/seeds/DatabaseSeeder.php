<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// Seeds must follow the following order to run correctly
		$this->call('CustomerSeeder');
		$this->call('CustomerItemSeeder');
		$this->call('WarehouseSeeder');
		$this->call('AisleSeeder');
		$this->call('BinLevelSeeder');
		$this->call('BinSeeder');
		$this->call('DrShipmentItemLocationSeeder');
		$this->call('PickAisleSeeder');
		$this->call('SectionSeeder');
		$this->call('LevelSeeder');
		$this->call('PpTablesSeeder');
		$this->call('UserSeeder');

		Model::reguard();

	}

}
