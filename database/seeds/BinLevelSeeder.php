<?php

use Illuminate\Database\Seeder;
use App\Models\BinLevel;
use App\Models\Bin;

class BinLevelSeeder extends Seeder {

    public function run() {
        DB::table('bin_levels')->truncate();

        $bins = Bin::get();

        foreach($bins as $bin) {
            foreach(range('a', 'c') as $index) {
              $binLevel = new BinLevel;

  						$binLevel->warehouse_id 		= 1;
  						$binLevel->aisle_id 				= $bin->aisle_id;
  						$binLevel->bin_id 					= $bin->id;
  						$binLevel->level 						= $index;
  						$binLevel->created_by 		 	= 20;
  						$binLevel->updated_by 		 	= 20;

  						$binLevel->save();
            }
        }
    }
}
