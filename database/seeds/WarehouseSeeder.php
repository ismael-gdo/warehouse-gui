<?php

use Illuminate\Database\Seeder;
use App\Models\Warehouse;

class WarehouseSeeder extends Seeder {

    public function run()
    {
        DB::table('warehouses')->truncate();

				$warehouse = new Warehouse;

				$warehouse->warehouse_name 			= 'Warehouse 1';
				$warehouse->warehouse_address1 	= '123 Main Address1';
				$warehouse->warehouse_address2 	= '123 Main Address2';
				$warehouse->warehouse_city 			= 'Main City';
				$warehouse->warehouse_state 		= 'AL';
				$warehouse->warehouse_zip 			= '12345';
				$warehouse->warehouse_country 	= 'US';
				$warehouse->warehouse_phone1 		= '1111111111';
				$warehouse->warehouse_phone2 		= '1111111111';
				$warehouse->manager_name 				= 'Manager1';
				$warehouse->manager_email 			= 'manager1@email.com';
				$warehouse->manager_phone1 			= '1111111111';
				$warehouse->manager_phone2 			= '1111111111';
				$warehouse->manager_address1 		= '123 Main Address1';
				$warehouse->manager_address2 		= '123 Main Address2';
				$warehouse->manager_city 				= 'Main City';
				$warehouse->manager_state 			= 'AL';
				$warehouse->manager_zip 				= '12345';
				$warehouse->manager_country 		= 'US';
				$warehouse->default_bin 				= '2';
				$warehouse->default_bin_level 	= '2';
				$warehouse->created_by 					= '1';
				$warehouse->updated_by 					= '1';

				$warehouse->save();
    }

}
