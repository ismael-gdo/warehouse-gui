@extends('app')

@section('pageTitle', $pageTitle)

@section('content')

<h1>{{ $pageTitle }}</h1>
<hr/>

<gui-view :inventory-data="{{ $inventoryData }}"></gui-view>

@stop
