import Vue from 'vue';

import GuiView from './vues/WarehouseGUI/GuiView.vue';

import moment from 'moment';
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

new Vue({

  el: '#app',

  data() {
    return {

    }
  },

  components: {
    GuiView
  }

});
